//
//  ViewController.swift
//  CloudKit Sharing
//
//  Created by Duong on 7/18/16.
//  Copyright © 2016 Duong. All rights reserved.
//

import UIKit
import CloudKit

/*
Program functionalities:
 - Create new record (row) include file from NSBunble Sample.Zip
 - Fetch records for current user.
 - Share a records to other user with a key.
 - Fetch a sharing record with a key.
 */


class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //MARK:- OUTLET
    @IBOutlet weak var tableView: UITableView!
    //MARK:- GLOBAL VARIBALE
    var isAuthenticated: Bool = false
    var currentUserID: String = ""
    var datasource: [Data] = []
    
    //MARK:- INPUT VARIABLE
    //MARK:- INIT METHOD
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        askUserToEnterCredential { (success) in
            if success {
                self.isAuthenticated = true
                //self.fetchRecordByIdentifiter()
                //self.fetchRecordByNSPredicate()
                self.getCurrentCKUser({ (id) in
                    self.currentUserID = id
                    self.fetchRecordByNSPredicate()
                })
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- ACTION
    @IBAction func tappedFetchButton() {
        fetchRecordByNSPredicate()
    }
    
    @IBAction func createNewRecord() {
        if isAuthenticated {
            saveRecord({ (success) in
                if success {
                    self.fetchRecordByNSPredicate()
                }
                else {
                    self.showOK("Error create new record")
                }
            })
        }
        else {
            askUserToEnterCredential({ (success) in
                
            })
        }
    }
    
    @IBAction func tappedFetchSharing() {
        self.showShareFetching()
    }
    
    //MARK:- FUNCTION
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell")
        cell?.textLabel?.text = datasource[indexPath.row].userID
        cell?.detailTextLabel?.text = datasource[indexPath.row].recordID
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        print(datasource[indexPath.row].description)
        //
        showShareCreation(datasource[indexPath.row].recordID)
    }
    
    //MARK: DATA
    
    /**
     Ask user to login iCloud.
     
     - parameter handler:
     */
    func askUserToEnterCredential(handler: (Bool) -> ()) {
        CKContainer.defaultContainer().accountStatusWithCompletionHandler { (status, error) in
            if status != CKAccountStatus.Available {
                let alert = UIAlertController(title: "Sign in to iCloud", message: "Sign in to your iCloud account to write records. On the Home screen, launch Settings, tap iCloud, and enter your Apple ID. Turn iCloud Drive on. If you don't have an iCloud account, tap Create a new Apple ID.", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Go to Settings", style: .Default, handler: { (action) in
                    UIApplication.sharedApplication().openURL(NSURL(string: UIApplicationOpenSettingsURLString)!)
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Cancel, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
                handler(false)
            }
            else {
                handler(true)
            }
        }
    }
    
    /**
     Get current cloud kit user id.
     
     - parameter completion:
     */
    func getCurrentCKUser(completion: (String) -> ()) {
        CKContainer.defaultContainer().fetchUserRecordIDWithCompletionHandler({ (recordID, error) in
            if let error = error {
                print(error.debugDescription)
            }
            else {
                print(recordID?.recordName)
                completion(recordID!.recordName)
            }
        })
    }
    
    
    /**
     Fetch record by identifier and save Sample file data.
     
     - parameter recordID:
     */
    func fetchRecordByIdentifiter(recordID: String) {
        let database = CKContainer.defaultContainer().publicCloudDatabase
        let recordID = CKRecordID(recordName: recordID)
        database.fetchRecordWithID(recordID) { (record, error) in
            if let error = error {
                print("Fetch record by identifier falied" + error.debugDescription)
            }
            else {
                let object = record!.objectForKey("backUpFile") as! CKAsset
                print(object.fileURL)
                //
                if let data = NSData(contentsOfURL: object.fileURL) {
                    //
                    let documents = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)[0] 
                    let url = NSURL(string: documents)
                    
                    
                    let saveFilePath = url!.path!.stringByAppendingString("/SampleFile.zip")
                    //
                    do {
                        try data.writeToURL(NSURL(fileURLWithPath: saveFilePath), options: NSDataWritingOptions.AtomicWrite)
                        //print(saveFilePath)
                        self.showOK("Fetch data success with file path: \(saveFilePath)")
                        print(saveFilePath)
                        
                        //Add new item here.
                        
                    }
                    catch let error as NSError {
                        print("Error: " + error.debugDescription)
                        self.showOK("Save data error with description:  \(error.debugDescription)")
                    }
                    
                    
                    print("Fetch")
                }
                else {
                    print("Cant fetch")
                }
            }
        }
    }
    
    /**
     Fetch data by query.
     */
    func fetchRecordByNSPredicate() {
        let database = CKContainer.defaultContainer().publicCloudDatabase
        let current = CKRecordID(recordName: self.currentUserID)
        let predicate = NSPredicate(format: "iCloudID == %@", current.recordName)
        let query = CKQuery(recordType: "Data", predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (record, error) in
            if let error = error {
                print("Query data error: " + error.debugDescription)
            }
            else {
                print("Query data successfull with data: \(record?.count)")
                guard let records = record else { return }
                self.datasource.removeAll()
                for element in records {
                    let data = Data(CKRecord: element)
                    self.datasource.append(data)
                }
                self.tableView.reloadData()
            }
        }
    }
    
    
    /**
     Create sharing on iCloud.
     
     - parameter checkSum: check sum
     - parameter recordID: record id
     */
    func saveSharing(checkSum: String, recordID: String) {
        let sharingRecord = CKRecord(recordType: "Sharing")
        sharingRecord["iCloudID"] = self.currentUserID
        sharingRecord["checksum"] = checkSum
        sharingRecord["recordId"] = recordID
        //
        let publicDatabase = CKContainer.defaultContainer().publicCloudDatabase
        //
        publicDatabase.saveRecord(sharingRecord) { (record, error) in
            if let error = error {
                print("Error save share." + error.description)
                 self.showOK("Error save share." + error.description)
            }
            else {
                print("Save share successfully")
                self.showOK("Let fetch data within your code: \(checkSum)")
            }
        }
    }
    
    
    /**
     Fetch sharing
     
     - parameter checkSum: key to check.
     */
    func fetchShareAndRecord(checkSum: String) {
        self.fetchSharing(checkSum) { (success, id) in
            if success {
                self.fetchRecordByIdentifiter(id!)
            }
            else {
                print("Fetch sharing error")
            }
        }
    }
    
    
    /**
     Fetch sharing.
     
     - parameter checkSum: checkSum
     - parameter handler:  handler
     */
    func fetchSharing(checkSum: String, handler: (Bool, String?) -> ()) {
        let database = CKContainer.defaultContainer().publicCloudDatabase
        let predicate = NSPredicate(format: "checksum == %@", checkSum)
        let query = CKQuery(recordType: "Sharing", predicate: predicate)
        database.performQuery(query, inZoneWithID: nil) { (record, error) in
            if let error = error {
                print("Query data error: " + error.debugDescription)
                handler(false, nil)
            }
            else {
                print("Query data successfull with data: \(record?.count)")
                guard let records = record else {
                    handler(false, nil)
                    return
                }
                //
                if let first = records.first {
                    handler(true, first.objectForKey("recordId") as? String)
                }
                else {
                    handler(false, nil)
                }
            }
        }
    }
    
    
    
    /**
     Save record.
     
     - parameter completion: completion.
     */
    func saveRecord(completion: (Bool)->()) {
        //let reference = CKRecordID(recordName: "Men chen")
        let artworkRecord = CKRecord(recordType: "Data")
        //let artistReference = CKReference(recordID: reference, action: .None)
        let string = NSURL(fileURLWithPath: NSBundle.mainBundle().pathForResource("Sample", ofType: "zip")!)
        //let url = NSURL(string: string)!
        let asset = CKAsset(fileURL: string)
    
        //Record filed
        artworkRecord["iCloudID"] = self.currentUserID;
        artworkRecord["backUpFile"] = asset
        
        
        let myContainer = CKContainer.defaultContainer()
        let publicDatabase = myContainer.publicCloudDatabase
        
        //Get private cloud.
        //let privateCloud = myContainer.privateCloudDatabase
        publicDatabase.saveRecord(artworkRecord) { (record, error) in
            if let error = error {
                print("Error save record." + error.description)
                completion(false)
            }
            else {
                print("Save record successfully")
                completion(true)
            }
        }
    }
    
    func copyDatabaseIntoDocumentDirectory() -> String {
        
        let documents = try! NSFileManager.defaultManager().URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false)
        let saveFilePath = documents.path!.stringByAppendingString("Sample.zip")
        let filePath = NSBundle.mainBundle().pathForResource("Sample", ofType: "zip")
        if NSFileManager.defaultManager().fileExistsAtPath(saveFilePath) == false {
            NSFileManager.defaultManager()
            try! NSFileManager.defaultManager().copyItemAtPath(filePath!, toPath: saveFilePath)
        }
        return saveFilePath
    }
    
    
    //MARK:- UI.
    func showShareCreation(recordID: String) {
        
        let alert = UIAlertController(title: "Create sharing", message: "Add your check sum key", preferredStyle: .Alert)
        let actionOK = UIAlertAction(title: "Create", style: UIAlertActionStyle.Default) { (action) in
            let checkSum = alert.textFields![0].text!
            self.saveSharing(checkSum, recordID: recordID)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Enter your check sum..."
        }
        
        alert.addAction(actionOK)
        alert.addAction(actionCancel)
        //
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func showShareFetching() {
        
        let alert = UIAlertController(title: "Fetch sharing", message: "Add your check sum key to get data back", preferredStyle: .Alert)
        let actionOK = UIAlertAction(title: "Get", style: UIAlertActionStyle.Default) { (action) in
            let checkSum = alert.textFields![0].text!
            self.fetchShareAndRecord(checkSum)
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        
        alert.addTextFieldWithConfigurationHandler { (textfield) in
            textfield.placeholder = "Enter your check sum..."
        }
        
        alert.addAction(actionOK)
        alert.addAction(actionCancel)
        //
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    
    func showOK(message: String) {
        showAlert(message, ok: "OK", cancel: "Cancel", completion: nil)
    }
    func showAlert(title: String, ok: String, cancel: String, completion: (() -> ())?) {
        let alertController = UIAlertController(title: title, message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: ok, style: UIAlertActionStyle.Default, handler: { (a) -> Void in
            alertController.dismissViewControllerAnimated(false, completion: nil)
            completion?()
        }))
        alertController.addAction(UIAlertAction(title: cancel, style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
}

class Data {
    var recordID: String!
    var userID: String!
    var file: CKAsset!
    var createdTime: NSDate!
    var modifiedTime: NSDate!
    
    init() {
        
    }
    
    init(CKRecord record: CKRecord) {
        //file = record.objectForKey("file") as! CKAsset
        userID = record.objectForKey("iCloudID") as! String
        file = record.objectForKey("backUpFile") as! CKAsset
        recordID = record.recordID.recordName
        createdTime = record.creationDate
        modifiedTime = record.modificationDate
    }
    
    var description: String {
        return "---- UserID: " + userID + "\nBackup file: " + file.fileURL.absoluteString
    }
}

